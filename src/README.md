**Pull exist docker image from docker hub: **

$ docker pull yingkailiang/xai

**Run Docker Image:** 

$ docker run -it -v $PWD:/home/app -u 0 --rm yingkailiang/xai bash



**Or Build Docker Image from scratch** 

$ docker build -t <image_name> .


**Install R package:** 

$ R

\> install.packages("remotes")

\> remotes::install_github("statsmaths/genlasso")

\> remotes::install_github("ggrothendieck/gsubfn")


**Run explainer AI:**

$ python3 xai.py 
