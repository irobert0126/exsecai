from keras.models import load_model
import pickle
from keras.preprocessing.sequence import pad_sequences
import numpy as np

if __name__ == "__main__":
    print('[Load model...]')
    model = load_model('target_model/O0_Bi_Rnn.h5')
    PATH_TEST_DATA = 'data/elf_x86_32_gcc_O0_test.pkl'
    n_fea_select = 25

    #PATH_TEST_DATA = 'elf_x86_32_gcc_O0_test.pkl'
    print('[Load data...]')
    data = pickle.load(open(PATH_TEST_DATA,'rb'))
    data_num = len(data[0])
    print('Data_num:',data_num)
    seq_len = len(data[0][0])
    print('Sequence length:', seq_len)

    ### Padding sequence ....
    x_test = pad_sequences(data[0], maxlen=seq_len, dtype='int32', padding='post', truncating='post', value=0)
    x_test = x_test + 1
    y = pad_sequences(data[1], maxlen=seq_len, dtype='int32', padding='post', truncating='post', value=0)
    y_test = np.zeros((data_num, seq_len, 2), dtype=y.dtype)
    for test_id in range(data_num):
        y_test[test_id, np.arange(seq_len), y[test_id]] = 1

    print(x_test.shape)
    print(y.shape)
    print(y_test.shape)

    idx = np.nonzero(y)[0]
    start_points = np.nonzero(y)[1]

    n1 = idx.shape[0]
    print(n1)
    n2 = start_points.shape[0]
    print(n2)

    n_pos = 0
    n_new = 0
    n_neg = 0

    n_pos_rand = 0
    n_new_rand = 0
    n_neg_rand = 0
    n = 0

    print(y[np.nonzero(y)])
    print(x_test[np.nonzero(y)])
    print(x_test[idx,start_points])
