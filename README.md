# eXSecAI - eXplainable AI on Security Products

## Explain
In the past year, we witnessed a dramatic rise in the platforms and apps based on machine learning and artificial intelligence. AI technology has impacted from software and the internet industry to other verticals such as healthcare, legal, manufacturing, automobile, and agriculture. This trend also applied to the security industry, all types of security products (e.g., malware detection engine, firewall IPS/IDS engine and endpoint sandbox) begin to leverage AI technology to escalate their power for better detection rate and quicker response speed. However, at the same time, due to the more and more complex model, especially some deep neural network based engines, it is extremely hard to understand the decision made by the products. Typical concerns including the reaction like **How on earth did the endpoint product quarantine my document?** from a frustrated customer, or a complaint like **How does the firewall missed such a naive XSS attack?** from Dev-Ops team. As a result, an accountable explanation is becoming the necessary part along with the 'binary' malicious-or-benign result.

The following Figure shows the explaination of our tool.

<img src='explain.png' width=1000 />


## Evaluation
* Explaining SQL Injection Attack.
    * <img src='paper/Fig/sql1.png' width=1000 />
    * <img src='paper/Fig/sql2.png' width=1000 />
    * <img src='paper/Fig/sql3.png' width=1000 />
    * <img src='paper/Fig/sql4.png' width=1000 />

* Explaining XSS Attack.
    * <img src='paper/Fig/XSS0.png' width=1000 />
    * <img src='paper/Fig/XSS1.png' width=1000 />
    * <img src='paper/Fig/XSS2.png' width=1000 />

* Explaining XXE Attack.
    * <img src='paper/Fig/XXE1.png' width=1000 />
    * <img src='paper/Fig/XXE2.png' width=1000 />

## Run
**Pull exist docker image from docker hub: **
$ docker pull yingkailiang/xai
Run Docker Image:
$ docker run -it -v $PWD:/home/app -u 0 --rm yingkailiang/xai bash
Or Build Docker Image from scratch
