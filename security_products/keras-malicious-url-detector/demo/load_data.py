import pickle
from keras.preprocessing.sequence import pad_sequences
import numpy as np

from keras.models import load_model
import pandas as pd
import os

### convert into 3 steps: load data, pre_process data, generate label
def load_url_data(data_dir_path):
    url_data = pd.read_csv(data_dir_path + os.path.sep + 'URL.txt', sep=',')
    url_data.columns = ['text', 'label']
    class_zero = url_data[url_data['label'] == 0].reset_index()
    #print("class_zero: \n",class_zero)
    class_one = url_data[url_data['label'] == 1].reset_index()
    #print("class_one: \n",class_one)
    class_zero = class_zero.truncate(before=1, after=class_one.shape[0])
    url_data = pd.concat([class_zero, class_one])
    url_data = url_data.sample(frac=1.0).reset_index()
    return url_data

def gen_url_x_test(url_data): 
    x_test=[]
    for url, label in zip(url_data['text'], url_data['label']):
        x_test.append(url)
    #print(x_test)
    return x_test

def gen_url_y_test(url_data): 
    y_test=[]
    for url, label in zip(url_data['text'], url_data['label']):
        y_test.append(label)
    #print(y_test)
    return y_test

def url_pre_process(url_data):
    x_test = gen_url_x_test(url_data) 
    en_x = encode_strings(x_test) # get one instance out 
    padding_result = url_padding_sequences(en_x)
    return padding_result

def url_padding_sequences(data):
    data_num = len(data)
    print('Data_num:',data_num)
    seq_len = len(data[0])
    print('Sequence length:', seq_len)

    ### Padding sequence ....
    print("data: ",data[0])
    print("data type: {}".format(type(data)))
    x_test = pad_sequences(data, maxlen=seq_len, dtype='int32', padding='post', truncating='post', value=0)
    #print("x_test: \n",x_test)
    return x_test

#rule to pick test instance 
def url_data_sampling(data): 
    return data[0:1]

def encode_strings(input_strings):
    ret = []
    for s in input_strings:
        r = encode_string(s)
        ret.append(r)
    return ret
    
def encode_string(input_string): 
    ret = [] 
    for c in input_string:
        ret.append(ord(c))
    return ret

def decode_ascii(input_string): 
    decodeMsg ='' 
    for item in input_string: 
        decodeMsg += chr(item)
    return decodeMsg

########################################################################################

def load_binary_data():
    PATH_TEST_DATA = 'data/elf_x86_32_gcc_O0_test.pkl'
    #PATH_TEST_DATA = 'elf_x86_32_gcc_O0_test.pkl'
    print('[Load data...]')
    data = pickle.load(open(PATH_TEST_DATA,'rb'))
    return data

def binary_pre_process(data):
    data_num = len(data[0])
    print('Data_num:',data_num)
    seq_len = len(data[0][0])
    print('Sequence length:', seq_len)

    ### Padding sequence ....
    print("data[0][0]: ",data[0][0])
    print("data type: {}".format(type(data)))
    x_test = pad_sequences(data[0], maxlen=seq_len, dtype='int32', padding='post', truncating='post', value=0)
    
    print("x_test type: {}".format(type(x_test)))
    print("x_test: ",x_test[0])
    x_test = x_test + 1
    return x_test

def binary_gen_label(data):
    data_num = len(data[0])
    print('Data_num:',data_num)
    seq_len = len(data[0][0])
    print('Sequence length:', seq_len)

    #print("x_test + 1: ",x_test[0:1])
    ### generate class for each data
    y = pad_sequences(data[1], maxlen=seq_len, dtype='int32', padding='post', truncating='post', value=0)
    y_test = np.zeros((data_num, seq_len, 2), dtype=y.dtype)
    for test_id in range(data_num):
        y_test[test_id, np.arange(seq_len), y[test_id]] = 1 #convert a binary decision into <0,1> or <1,0>
    print("y_test shape: ",y_test.shape) 
    print("y shape: ",y.shape) 
    return y
    
# wrap all sample extraction logic 
# output an array of unit sample for xai to explain
def binary_data_sampling(x_test,y): 
    idx = np.nonzero(y)[0]
    start_points = np.nonzero(y)[1]
    n = 0
    ret = []
    for i in range(len(x_test)):
        if i in idx:
            print('\n------>', i)
            idx_Col = np.where(idx == i)
            idx_Row = start_points[idx_Col]
            print('start_points: ',start_points)
            print('idx_Col ...',idx_Col)
            print('idx_Row ...', idx_Row)
            binary_func_start = x_test[i][idx_Row]
            print('binary_func_start ...', x_test[i][idx_Row])
            x_test_d = x_test[i:i + 1]
            for j in range(len(idx_Row)):
                return x_test_d, binary_func_start[j], idx_Row[j] 




if __name__ == "__main__":
    #model = load_model('target_model/O0_Bi_Rnn.h5')
    #data = load_binary_data()
    #x_test = binary_pre_process(data)
    #y = binary_gen_label(data)
    #x_test_d, binary_func_start, idx_row = binary_data_sampling(x_test,y)
    #print("binary_func_start: ", binary_func_start)
    #print("idx_row: ", idx_row)

    url_data = load_url_data('./data')
    #padding_result = url_pre_process(url_data)
    #x_test = url_data_sampling(padding_result)
    #print(x_test)
