from rpy2 import robjects
from rpy2.robjects.packages import importr
import rpy2.robjects.numpy2ri
import sys
import rpy2.robjects.packages as rpackages
import os
#os.environ["THEANO_FLAGS"] = "device=gpu,floatX=float32"
import numpy as np
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from scipy import io
import pickle
import load_data as ld
import load_model as lm

np.random.seed(1234)

r = robjects.r
rpy2.robjects.numpy2ri.activate()

#TODO: install R packages from github 
importr('genlasso')
importr('gsubfn')

class xai_rnn(object):

    """class for explaining the rnn prediction"""
    def __init__(self, model, data, start_binary, real_start_sp):
        """
        Args:
            model: target rnn model.
            data: data sample needed to be explained.
            label: label of the data sample.
            start: value of function start.
        """
        self.model = model
        self.data = data
        self.seq_len = data.shape[1]
        self.seq_len = data[0].shape[0]
        self.start = start_binary
        self.sp = np.where((self.data == self.start))
        self.real_sp = real_start_sp
        self.pred = self.model.predict(self.data, verbose = 0)[self.sp]

        #print 'Seq_len ... ', self.seq_len
        #print 'Start ...', self.start
        #print 'Sp ...',self.sp
        #print 'Real_start_sp ...',self.real_sp
        #print 'Pred ...\n',self.pred


    def truncate_seq(self, trunc_len):
        """ Generate truncated data sample
        Args:
            trun_len: the lenght of the truncated data sample.

        return:
            trunc_data: the truncated data samples.
        """
        self.trunc_data_test = np.zeros((1, self.seq_len), dtype=int)
        #self.trunc_data_test = np.ones((1, self.seq_len),dtype = int)
        self.tl = trunc_len
        cen = self.seq_len/2
        half_tl = trunc_len/2

        if self.real_sp < half_tl:
            #print 'self.real_sp < half_tl'
            self.trunc_data_test[0, int(cen - self.real_sp):int(cen)] = self.data[0, 0:int(self.real_sp)]
            self.trunc_data_test[0, int(cen):int(cen+half_tl+1)] = self.data[0, int(self.real_sp):int(self.real_sp+half_tl+1)]

        elif self.real_sp >= self.seq_len - half_tl:
            #print 'self.real_sp >= self.seq_len - half_tl:'
            self.trunc_data_test[0, int(cen - half_tl):int(cen)] = self.data[0, int(self.real_sp-half_tl):int(self.real_sp)]
            self.trunc_data_test[0, int(cen):int(cen + (self.seq_len-self.real_sp))] = self.data[0, int(self.real_sp):int(self.seq_len)]

        else:
            print ('trunc_data_test: {}:{}, {}:{}'.format(int(cen - half_tl),int(cen + half_tl + 1),int(self.real_sp - half_tl),int(self.real_sp + half_tl + 1)))
            self.trunc_data_test[0, int(cen - half_tl):int(cen + half_tl + 1)] = self.data[0, int(self.real_sp - half_tl):int(self.real_sp + half_tl + 1)]

        print("self.data {}: {}".format(len(self.data),self.data))
        self.trunc_data = self.trunc_data_test[0, int(cen - half_tl):int(cen + half_tl + 1)]
        print("self.trunc_data {}: {}".format(len(self.trunc_data),self.trunc_data))
        return self.trunc_data


    def pos_bootstrap_trun(self):
        """ Generate positive bootstrap sample and test it
        return:
            test_data: generated positive boostrap sample
            P_pos: prediction probability
        """
        cen = self.seq_len/2
        half_tl = self.tl/2
        test_data = np.copy(self.data)

        if self.real_sp < half_tl:
            test_data[0, 0:self.real_sp] = 0
            test_data[0, (self.real_sp+1):(self.real_sp+half_tl+1)] = 0

        elif self.real_sp >= self.seq_len - half_tl:
            test_data[0, (self.real_sp-half_tl):self.real_sp] = 0
            test_data[0, (self.real_sp+1):self.seq_len] = 0

        else:
            test_data[0, (self.real_sp - half_tl):self.real_sp] = 0
            test_data[0, (self.real_sp+1):(self.real_sp+half_tl+1)] = 0
        P_pos = self.model.predict(test_data, verbose=0)[0, self.real_sp, 1]
        #print("model.predict: {}".format(P_pos))

        return test_data, P_pos


    def neg_bootstrap_trun(self, test_seed, neg_pos):
        """ Generate negative bootstrap sample and test it
        Arg:
            test_seed: seed for negative bootstrap sample
        return:
            neg_data: generated negative bootstrap sample
            P_neg: prediction probability
        """
        test_data = np.copy(test_seed)
        pos = neg_pos
        cen = self.seq_len/2
        half_tl = self.tl/2
        if pos < half_tl:
            test_data[0, 0:pos] = self.trunc_data[(half_tl - pos):half_tl]
            test_data[0, (pos+1):(pos+half_tl+1)] = self.trunc_data[(half_tl+1):]

        elif pos >= self.seq_len - half_tl:
            test_data[0, (pos-half_tl):pos] = self.trunc_data[0:half_tl]
            test_data[0, (pos+1):self.seq_len] = self.trunc_data[(half_tl+1):(half_tl+self.seq_len - pos)]
        else:
            test_data[0,(pos - half_tl):pos] = self.trunc_data[ 0:half_tl]
            test_data[0,(pos+1):(pos+half_tl+1)] = self.trunc_data[(half_tl+1):]
        P_neg = self.model.predict(test_data, verbose=0)[0, pos, 1]
        #print("model.predict: {}".format(P_neg))
        return test_data, P_neg

    def new_testing_trun(self):
        """ Generate new testing sample and test it
        return:
            new_data: generated negative bootstrap sample
            P: prediction probability
        """
        P = self.model.predict(self.trunc_data_test, verbose=0)[0, 100, 1]
        #print("model.predict: {}".format(P))

    def xai_feature(self, samp_num, option= 'None'):
        """extract the important features from the input data
        Arg:
            fea_num: number of features that needed by the user
            samp_num: number of data used for explanation
        return:
            fea: extracted features
        """
        print("=======================================")
        print("         start xai_feature()           ")
        print("=======================================")
        cen = self.seq_len/2
        half_tl = self.tl/2
        sample = np.random.randint(1, self.tl+1, samp_num)
        features_range = range(self.tl+1)
        data_explain = np.copy(self.trunc_data).reshape(1, self.trunc_data.shape[0])

        print("samp_num: ",samp_num)
        print("seq_len: {}".format(self.seq_len))
        print("seq_tl: {}".format(self.tl))
        print("sample {}: {}".format(len(sample),sample))
        print("features_range : {}".format(features_range))

        print("data_explained {} --> X: {}".format(len(data_explain), data_explain))
        data_sampled = np.copy(self.trunc_data_test)
        print("data_sampled {}: {}".format(len(data_sampled),data_sampled))
        for i, size in enumerate(sample, start=1):
            inactive = np.random.choice(features_range, size, replace=False)
            #print('\ninactive --->',inactive)
            tmp_sampled = np.copy(self.trunc_data)
            tmp_sampled[inactive] = 0
            #tmp_sampled[inactive] = np.random.choice(range(257), size, replace = False)
            tmp_sampled = tmp_sampled.reshape(1, int(self.trunc_data.shape[0]))
            data_explain = np.concatenate((data_explain, tmp_sampled), axis=0)
            data_sampled_mutate = np.copy(self.data)
            if self.real_sp < half_tl:
                data_sampled_mutate[0, 0:int(tmp_sampled.shape[1])] = tmp_sampled
            elif self.real_sp >= self.seq_len - half_tl:
                data_sampled_mutate[0, int(self.seq_len - tmp_sampled.shape[1]): int(self.seq_len)] = tmp_sampled
            else:
                data_sampled_mutate[0, int(self.real_sp - half_tl):int(self.real_sp + half_tl + 1)] = tmp_sampled
            data_sampled = np.concatenate((data_sampled, data_sampled_mutate),axis=0)

        if option == "Fixed":
            print("Fix start points")
            data_sampled[:, self.real_sp] = self.start
        label_sampled = self.model.predict(data_sampled, verbose = 0)[:, self.real_sp, 1]
        label_sampled = label_sampled.reshape(label_sampled.shape[0], 1)
        #print("model.predict: {}".format(label_sampled))
        X = r.matrix(data_explain, nrow = data_explain.shape[0], ncol = data_explain.shape[1])
        Y = r.matrix(label_sampled, nrow = label_sampled.shape[0], ncol = label_sampled.shape[1])

        n = r.nrow(X)
        p = r.ncol(X)
        results = r.fusedlasso1d(y=Y,X=X)
        result = np.array(r.coef(results, np.sqrt(n*np.log(p)))[0])[:,-1]

        importance_score = np.argsort(result)[::-1]
        print('\nimportance_score ...\n',importance_score)
        self.fea = (importance_score-self.tl/2)+self.real_sp
        self.fea = self.fea[np.where(self.fea<200)]
        self.fea = self.fea[np.where(self.fea>=0)]
        print('\nself.fea ...\n',self.fea)
        print("=======================================")
        print("         end  xai_feature()            ")
        print("=======================================")
        return self.fea


if __name__ == "__main__":
    model = lm.url_load_model()
    n_fea_select = 25

    print('[Load data...]')
    data = ld.load_url_data('./data')
    x_test = ld.url_pre_process(data)
    #y = ld.binary_gen_label(data)

    #idx = np.nonzero(y)[0]
    #start_points = np.nonzero(y)[1]

    n = 0

    x_test_d= ld.url_data_sampling(x_test)
    binary_func_start =[]
    idx_Row=[]
    binary_func_start.append(5)
    idx_Row.append(5)
    xai_test = xai_rnn(model, x_test_d, binary_func_start, idx_Row)
    print("xai test pred[0,1]: ",xai_test.pred[0, 1])
    if xai_test.pred[0, 1] > 0.5:
        truncate_seq_data = xai_test.truncate_seq(40)
        xai_fea = xai_test.xai_feature(500)
        fea = np.zeros_like(xai_test.data)
        tmp_xai_fea = [0] * 25
        len_xai_fea = min(25,len(xai_fea))
        for i in range(0,len_xai_fea):
            tmp_xai_fea[i] = int(xai_fea[i]) 
        #xai_fea[0:25] = tmp_xai_fea[0:25]
        fea[0, tmp_xai_fea[0:25]] = xai_test.data[0, tmp_xai_fea[0:25]]
        print("fea:\n ",fea)
