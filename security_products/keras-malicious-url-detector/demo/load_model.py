from keras_malicious_url_detector.library.bidirectional_lstm import BidirectionalLstmEmbedPredictor
from keras_malicious_url_detector.library.utility.url_data_loader import load_url_data

def url_load_model(): 
    model_dir_path = './models'
    predictor = BidirectionalLstmEmbedPredictor()
    predictor.load_model(model_dir_path)
    return predictor
    
def test(urls, labels):
    predictor = url_load_model()
    count = 0
    ret = []
    for url, label in zip(urls, labels): 
        predicted_label = predictor.predict_p(url)
        ret.append(predicted_label)
        count += 1
        if(count>20): 
            break
    print("total: ",count)
    return ret 

def main():
    data_dir_path = './data'
    url_data = load_url_data(data_dir_path)
    urls = url_data['text'] 
    labels = url_data['label']
    ret = test(urls,labels)
    print(ret)    
    

if __name__ == '__main__':
    main()
